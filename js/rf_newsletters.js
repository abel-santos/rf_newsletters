(function ($, Drupal) {
  Drupal.behaviors.rfNewsletterBehavior = {
    attach: function (context, settings) {
      $('input#edit-submit').val('Subscribe now');
      $('button.ui-dialog-titlebar-close', context).once('closeFormBehaviour').click(function () {
        $('div#block-newsletter').css('display', 'none');
      });
      $('input#edit-submit', context).click(function () {
        $('div#block-newsletter').css('display', 'none');
      });

      $(document, context).once('loadForm').each(function() {
        var name = $('#edit-title-0-value').val();
        if (name != 'undefined') {
          var valueName = $('#edit-title-0-value').val();
          if (valueName.length > 0) {
            // After submission, fild has info, so hide the form.
              $('div#block-newsletter').css('display', 'none');
          }
        }

      });
    }
  };
})(jQuery, Drupal);
