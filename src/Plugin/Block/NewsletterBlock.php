<?php

namespace Drupal\rf_newsletters\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'NewsletterBlock' block.
 *
 * @Block(
 *  id = "newsletter_block",
 *  admin_label = @Translation("Newsletter"),
 * )
 */
class NewsletterBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'block-title' => "Newsletter",
      'slogan' => "I'm a simple demo slogan.",
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    // Title of the block.
    $form['block-title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Newsletter Block Sub-Title'),
      '#description' => $this->t('Add the title that will be showed on the block. Max length is 100 characters.'),
      '#default_value' => $this->configuration['block-title'],
      '#maxlength' => 100,
      '#size' => 70,
      '#weight' => '40',
      '#required' => TRUE,
    ];
    // Slogan of the block.
    $form['slogan'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Slogan'),
      '#description' => $this->t('Add the slogan that will be showed on the block. Max length is 140 characters. Leave empty to not show it on the block.'),
      '#default_value' => $this->configuration['slogan'],
      '#maxlength' => 140,
      '#size' => 70,
      '#weight' => '50',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['slogan'] = $form_state->getValue('slogan');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Initialize build array.
    $build = [];
    // Get the form of subscriptions
    $values = array('type' => 'subscription');
    $node = \Drupal::entityTypeManager()
      ->getStorage('node')
      ->create($values);
    $form = \Drupal::entityTypeManager()
      ->getFormObject('node', 'default')
      ->setEntity($node);
    // Return render array
    // Set the theme for the block.
    $build['#theme'] = 'newsletter_block';
    // Add content.
    $build['#content']['block_title'] = $this->configuration['block-title'];
    $build['#content']['slogan'] = $this->configuration['slogan'];
    // Render and set he form.
    $build['#form'] = \Drupal::formBuilder()->getForm($form);
    // Return form's build.
    return $build;
  }

}
